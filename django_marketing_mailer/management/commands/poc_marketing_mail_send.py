from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Send Marketing Mail"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
